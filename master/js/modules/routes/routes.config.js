/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.routes')
    .config(routesConfig);

  routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];
  function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper){

    // Set the following to true to enable the HTML5 Mode
    // You may have to set <base> tag in index and a routing configuration in your server
    $locationProvider.html5Mode(false);

    // defaults to dashboard
    $urlRouterProvider.otherwise('/page/login');
    //'toastr',

    $stateProvider
      .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: helper.basepath('app.html'),
      resolve: helper.resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'whirl', 'al-ui', 'ngProgress', 'ab-base64','angularFileUpload', 'ngFileUpload', 'flot-chart','flot-chart-plugins', 'ui.select', 'ngDialog', 'ui.utils.masks')
    })
      .state('app.home', {
      url: '/home',
      title: 'home',
      templateUrl: helper.basepath('home.html'),
      resolve: helper.resolveFor( 'weather-icons')
    })
      .state('app.template', {
      url: '/template',
      title: 'Blank Template',
      templateUrl: helper.basepath('template.html')
    })
    
    //FUNCIONARIO
	   .state('app.funcionario', {
      url: '/funcionario',
      controller : 'funcionarioListController',
      templateUrl: 'app/views/funcionario/funcionario_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
	
	 .state('app.funcionario_add', {
      url: '/funcionario_add',
      controller : 'funcionarioController',
      templateUrl: 'app/views/funcionario/funcionario_form.html',
      params : { funcionario: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      .state('app.funcionario_edit', {
      url: '/funcionario_edit',
      controller : 'funcionarioController',
      templateUrl: 'app/views/funcionario/funcionario_form.html',
      params : { funcionario: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    //TERCERO
       .state('app.tercero', {
      url: '/tercero',
      controller : 'terceroListController',
      templateUrl: 'app/views/tercero/tercero_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
     .state('app.tercero_add', {
      url: '/tercero_add',
      controller : 'terceroController',
      templateUrl: 'app/views/tercero/tercero_form.html',
      params : { tercero: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
      .state('app.tercero_edit', {
      url: '/tercero_edit',
      controller : 'terceroController',
      templateUrl: 'app/views/tercero/tercero_form.html',
      params : { tercero: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
    
    //REPORTES    
     .state('app.reporte', {
      url: '/reporte',
      controller : 'reporteListController',
      templateUrl: 'app/views/reporte/reporte_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
    
    //OPORTUNIDAD
        
     .state('app.oportunidad', {
      url: '/oportunidad',
      controller : 'oportunidadListController',
      templateUrl: 'app/views/oportunidad/oportunidad_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
     .state('app.oportunidad_add', {
      url: '/oportunidad_add',
      controller : 'oportunidadController',
      templateUrl: 'app/views/oportunidad/oportunidad_form.html',
      params : { oportunidad: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.oportunidad_edit', {
      url: '/oportunidad_edit',
      controller : 'oportunidadController',
      templateUrl: 'app/views/oportunidad/oportunidad_form.html',
      params : { oportunidad: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })      
    
    //CONTRATO    
     .state('app.contrato', {
      url: '/contrato',
      controller : 'contratoListController',
      templateUrl: 'app/views/contrato/contrato_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.contrato_add', {
      url: '/contrato_add',
      controller : 'contratoController',
      templateUrl: 'app/views/contrato/contrato_form.html',
      params : { contrato: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html'),ok: helper.basepath('templates/ok_confirmation.html') }; }
      })
    })    
      .state('app.contrato_edit', {
      url: '/contrato_edit',
      controller : 'contratoController',
      templateUrl: 'app/views/contrato/contrato_form.html',
      params : { contrato: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html'),ok: helper.basepath('templates/ok_confirmation.html') }; }
      })
    })    
    
    //PRODUCTO
    
     .state('app.producto', {
      url: '/producto',
      controller : 'productoListController',
      templateUrl: 'app/views/producto/producto_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.producto_add', {
      url: '/contrato_add',
      controller : 'productoController',
      templateUrl: 'app/views/producto/producto_form.html',
      params : { producto: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.producto_edit', {
      url: '/producto_edit',
      controller : 'productoController',
      templateUrl: 'app/views/producto/producto_form.html',
      params : { producto: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    // CALENDARIO
    .state('app.calendar', {
          url: '/calendar',
          title: 'Calendar',
          templateUrl: helper.basepath('/calendar/calendar.html'),
          resolve: helper.resolveFor('jquery-ui', 'jquery-ui-widgets', 'moment', 'fullcalendar')
      })
    
    
    //AGENDAS    
     .state('app.agenda', {
      url: '/agenda',
      controller : 'agendaListController',
      templateUrl: 'app/views/agenda/agenda_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.agenda_add', {
      url: '/agenda_add',
      controller : 'agendaFormController',
      templateUrl: 'app/views/agenda/agenda_form.html',
      params : { agenda: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.agenda_edit', {
      url: '/agenda_edit',
      controller : 'agendaFormController',
      templateUrl: 'app/views/agenda/agenda_form.html',
      params : { agenda: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    

    //CONSECUTIVO
    
     .state('app.consecutivo', {
      url: '/consecutivo',
      controller : 'consecutivoListController',
      templateUrl: 'app/views/consecutivo/consecutivo_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.consecutivo_add', {
      url: '/consecutivo_add',
      controller : 'consecutivoController',
      templateUrl: 'app/views/consecutivo/consecutivo_form.html',
      params : { consecutivo: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.consecutivo_edit', {
      url: '/consecutivo_edit',
      controller : 'consecutivoController',
      templateUrl: 'app/views/consecutivo/consecutivo_form.html',
      params : { consecutivo: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
     .state('app.setMenu', {
      url: '/Menu',
      controller : 'setMenuController',
      templateUrl: 'app/views/menu/setMenu.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
     .state('app.compromiso', {
      url: '/Compromiso',
      controller : 'compromisoController',
      templateUrl: 'app/views/compromiso/setCompromiso.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
      .state('app.rol', {
      url: '/Roles',
      controller : 'rolController',
      templateUrl: 'app/views/roles/roles.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
        
    //FACTURA
    
     .state('app.factura', {
      url: '/factura',
      controller : 'facturaListController',
      templateUrl: 'app/views/factura/factura_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.factura_add', {
      url: '/factura_add',
      controller : 'facturaController',
      templateUrl: 'app/views/factura/factura_form.html',
      params : { factura: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.factura_edit', {
      url: '/factura_edit',
      controller : 'facturaController',
      templateUrl: 'app/views/factura/factura_form.html',
      params : { factura: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
    
    //MENSAJERIA
    
     .state('app.mensajeria', {
      url: '/mensajeria',
      controller : 'mensajeriaListController',
      templateUrl: 'app/views/mensajeria/mensajeria_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
      .state('app.mensajeria_add', {
      url: '/mensajeria_add',
      controller : 'mensajeriaController',
      templateUrl: 'app/views/mensajeria/mensajeria_form.html',
      params : { mensajeria: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })    
      .state('app.mensajeria_edit', {
      url: '/mensajeria_edit',
      controller : 'mensajeriaController',
      templateUrl: 'app/views/mensajeria/mensajeria_form.html',
      params : { mensajeria: null },
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })  
     
      .state('app.registroUbicacion', {
      url: '/registroUbicacion',
      controller : 'registroUbicacionListController',
      templateUrl: 'app/views/usuario/registroUbicacion_list.html',
      params : { filters : {}, data : []},
        resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    }) 
    //TAREA
    .state('app.tarea', {
      url: '/tarea',
      controller : 'tareaListController',
      templateUrl: 'app/views/tarea/tarea_list.html',
      params : { filters : {}, data : []},
      resolve: angular.extend(helper.resolveFor('ngDialog'), {
        tpl: function() { return { path: helper.basepath('templates/delete_confirmation.html') }; }
      })
    })
    
    // 
    // Single Page Routes
    // -----------------------------------
      .state('page', {
      url: '/page',
      templateUrl: 'app/pages/page.html',
      resolve: helper.resolveFor('modernizr', 'icons'),
      controller: ['$rootScope', function($rootScope) {
        $rootScope.app.layout.isBoxed = false;
      }]
    })
      .state('page.login', {
      url: '/login',
      controller : 'LoginController',
      title: 'Login',
      templateUrl: 'app/views/security/login.html'
    })
      .state('page.register', {
      url: '/register',
      title: 'Register',
      templateUrl: 'app/views/security/register.html'
    })
      .state('page.recover', {
      url: '/recover',
      controller : 'RecoverController',
      title: 'Recover',
      templateUrl: 'app/views/security/recover.html'
    })
      .state('page.resetpassword', {
      url: '/resetpassword/:token',
      controller : 'ResetPasswordController',
      templateUrl: 'app/views/security/resetPassword.html'
    })
      .state('page.lock', {
      url: '/lock',
      title: 'Lock',
      templateUrl: 'app/views/security/lock.html'
    })
      .state('page.404', {
      url: '/404',
      title: 'Not Found',
      templateUrl: 'app/pages/404.html'
    });

  } // routesConfig

})();

