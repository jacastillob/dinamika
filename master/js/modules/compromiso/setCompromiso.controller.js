/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.compromiso')
    .controller('compromisoController', compromisoController);

  compromisoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'tpl', 'ngDialog', 'parametersOfState', 'REGULAR_EXPRESION', 'compromisoRolHttp', 'message', '$rootScope'];


  function compromisoController($scope, $filter, $state, LDataSource, $modal, tpl, ngDialog, parametersOfState, REGULAR_EXPRESION, compromisoRolHttp, message, $rootScope){
    $scope.nombre='';
    $scope.compromisoId=0;
    $scope.nombreCompromiso='';
    $scope.compromiso= {
        current : {
          nombre:'',
          compromisoId:0
        },
        data:[],
        getData: function(){
            $scope.nombre='';
            $scope.compromisoId=0;
            $scope.roles.data=[];
            $rootScope.loadingVisible = true;            
           
            var parametros= {
                "tipo":$scope.tipo.current.value                
            };          
            compromisoRolHttp.getListCompromiso({},parametros,function(response) {                
                
                $scope.compromiso.data = $filter('orderBy')(response, 'compromisoId');               
                for(var i = 0; i < $scope.compromiso.data.length; i++){
                    
                    $scope.compromiso.data[i].subCompromiso = $filter('orderBy')($scope.compromiso.data[i].subCompromiso, 'compromisoId');
                    $scope.compromiso.data[i].nombre= $scope.compromiso.data[i].nombre;
                    $scope.compromiso.data[i].show=false;
                    
                    if($scope.compromiso.data[i].subCompromiso!=null){
                        for(var j=0; j<$scope.compromiso.data[i].subCompromiso.length; j++){                            
                            $scope.compromiso.data[i].subCompromiso[j].show=false;
                            if($scope.compromiso.data[i].subCompromiso[j].subCompromiso!=null){
                                $scope.compromiso.data[i].subCompromiso[j].subCompromiso = $filter('orderBy')($scope.compromiso.data[i].subCompromiso[j].subCompromiso, 'compromisoId');
                                
                                for(var k=0; k<$scope.compromiso.data[i].subCompromiso[j].subCompromiso.length; k++){
                                    $scope.compromiso.data[i].subCompromiso[j].subCompromiso[k].show=false;
                                }
                            }
                        }
                    }
                    
                }
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $scope.compromiso.data = [];
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los compromisos: " + faild.Message);
            })
        }, edit: function (item){
            var parametros={
              detalle:{},
              edit:true,
              nombre:'Menú'
            };
            parametros.detalle=item;
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/compromiso/compromiso_add.html',
                controller: 'compromisoAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {});
        }, addsubCompromiso: function (item){
            var parametros={
              detalle:{
                nombre: '',
                tipo: $scope.tipo.current.value,
                tipoPeriodo: '',            
                orden: 0,
                padreId: item.compromisoId,                
                subCompromiso: null,
                excel:0,
                word:0,
                pdf:0
              },
              edit:false,
              nombre:'Submenú'
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/compromiso/compromiso_add.html',
                controller: 'compromisoAddController',
                size: 'md',
                resolve: {
                 parameters: function () { return parameter; }                 
                }
            });
            modalInstance.result.then(function (parameters) {$scope.compromiso.getData();}, function (parameters) {$scope.compromiso.getData();});
            
        }, addCompromiso: function (){
            var parametros={
              detalle:{
                nombre: '',
                tipo: $scope.tipo.current.value,
                tipoPeriodo: '',            
                orden: 0,
                padreId: 0,                
                subCompromiso: null,
                excel:0,
                word:0,
                pdf:0
              },
              edit:false,
              nombre:'Menú',
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/compromiso/compromiso_add.html',
                controller: 'compromisoAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }                  
                }
            });
            modalInstance.result.then(function (parameters) {$scope.compromiso.getData();}, function (parameters) {$scope.compromiso.getData();});
            
        }, getRol: function(item){
            $scope.nombre=item.nombre;
            $scope.compromisoId=item.compromisoId;
            $scope.roles.getData();
            $scope.nombreCompromiso=item.nombre;
        }, remove: function(item){
            ngDialog.openConfirm({
                template: tpl.path,
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function (value) {              
                
                var id = item.compromisoId;               
                compromisoRolHttp.removeCompromiso({}, { id: id }, function(response) {
                    $scope.compromiso.getData();                    
                }, function(faild) {
                      $rootScope.loadingVisible = false;
                      message.show("error", "No se eliminó el Compromiso: " + faild.Message);
                });
                
                
            });
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        compromisoId: 0,
        getData: function(){
            $rootScope.loadingVisible = true;
            compromisoRolHttp.getCompromisoRol({},{ id : $scope.compromisoId}, function (response) {
                $scope.roles.data = response;
                $scope.roles.compromisoId = $scope.compromisoId;
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        },
        checkRol: function(item){
            var _Http = new compromisoRolHttp(item);
            $rootScope.loadingVisible = true;
            if(item.seleccionado==true){
              $rootScope.loadingVisible = true;
              _Http.$assignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Asignó el Rol: " + faild.Message);
              })
            }
            else{
              _Http.$unassignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Desasignó el Rol: " + faild.Message);
              })
            }
        }
    }
     //CARGAMOS PROCESOS
    $scope.tipo = {
        current : {}, data:[],
        getData : function() {            
            $scope.tipo.data.push({value:'FI', descripcion: 'Financiera'});
            $scope.tipo.data.push({value:'LE', descripcion: 'Legal'});
            $scope.tipo.data.push({value:'AD', descripcion: 'Administrativa'});       
            $scope.tipo.data.push({value:'SI', descripcion: 'Sistemas'});       
            $scope.tipo.current =$scope.tipo.data[0];
        }        
    }	   
    $scope.tipo.getData();      
    $scope.compromiso.getData();
  }
})();