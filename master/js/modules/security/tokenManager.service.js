
(function() {
  'use strict';

  angular
    .module('app.security')
    .service('tokenManager', tokenManager);

  tokenManager.$inject = ['$window', '$rootScope'];

  function tokenManager($window, $rootScope) {
    return ({
      set: setToken,
      get: getToken,
      getRefreshToken : getRefreshToken, 
      resetToken : resetToken
    });

    var token = '';

    function expirationTime(time) {
      console.log("expirationTime" + time);
      //var _time = (time * 60) - 60;
      var _time = time * 3;
      setTimeout(function() {
        $rootScope.$broadcast('refresh_token', { refreshToken : $window.localStorage.token } );
      }, _time * 1000);
    }

    function setToken(_token) {
      $rootScope.perfil = _token.perfil;
      token = _token.token;
      $window.localStorage.token = _token.refreshToken;
      expirationTime(_token.timeOut);
    }

    function getToken() {
      return token;
    }

    function getRefreshToken() {
      return $window.localStorage.token;
    }

    function resetToken() {
      token = '';
      $window.localStorage.removeItem("token");
    }


  }

})();



