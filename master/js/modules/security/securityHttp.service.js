/**=========================================================
 * Module: app.report.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.security')
    .service('securityHttp', securityHttp);

  securityHttp.$inject = ['$resource', 'END_POINT'];


  function securityHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };

    var actions = {
      'login' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/authUser'
      },
      'refreshToken' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/refreshToken'        
      },
      'resetPassword' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/resetPassword'        
      },
      'confirmPassword' : {
        'method' : 'POST',
        'url' : END_POINT + '/Administracion.svc/confirmPassword'        
      }
    };

    return $resource(END_POINT + '/Administracion.svc/authUser', paramDefault, actions);
  }

})();