/**=========================================================
 * Module: demo-pagination.js
 * Provides a simple demo for pagination
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.mailbox')
        .controller('MailboxController', MailboxController);

    function MailboxController() {
        var vm = this;

        activate();

        ////////////////

        function activate() {
          vm.folders = [
            {name: 'Bandeja Entrada',   folder: 'inbox',   alert: 42, icon: 'fa-inbox' },
            {name: 'Notificaciones', folder: 'notificacion', alert: 10, icon: 'fa-bell-o' },
            {name: 'Enviados',    folder: 'enviados',    alert: 0,  icon: 'fa-paper-plane-o' },
            {name: 'Destacados',   folder: 'destacados',   alert: 5,  icon: 'fa-star-o' },
            {name: 'Eliminados',   folder: 'eliminados',   alert: 0,  icon: 'fa-trash-o'}
          ];

          /*
          vm.labels = [
            {name: 'Red',     color: 'danger'},
            {name: 'Pink',    color: 'pink'},
            {name: 'Blue',    color: 'info'},
            {name: 'Yellow',  color: 'warning'}
          ];
          */

          vm.mail = {
            cc: false,
            bcc: false
          };
          
          
          // Mailbox editr initial content
          vm.content = '<p>Type something..</p>';
          
        }
    }
})();
