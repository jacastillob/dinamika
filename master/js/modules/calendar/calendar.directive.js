(function() {
    'use strict';

    angular
        .module('app.calendar')
        .directive('calendar', calendar);

    calendar.$inject = [ '$rootScope','calendarHttp','message','$modal'];
    
    function calendar ($rootScope,calendarHttp,message,$modal) {
            
        var directive = {
            link: link, 
            restrict: 'EA'        
        };  
        return directive;  
        
        function link(scope, element) {
          
          if(!$.fn.fullCalendar) return;  
            
            var calendar = element;            
            initCalendar(calendar, null, $rootScope.app.layout.isRTL,calendarHttp,$rootScope,message,$modal);
                    
        }  
    }
      /**
     * Invoke full calendar plugin and attach behavior
     * @param  jQuery [calElement] The calendar dom element wrapped into jQuery
     * @param  EventObject [events] An object with the event list to load when the calendar displays
     */
    function initCalendar(calElement, events, isRTL,calendarHttp,$rootScope,message,$modal) { 
        
           $rootScope.loadingVisible = true;
           calendarHttp.getAgendas({}, {}, function(response) {        
               
                    message.show("success", "Agendas cargadas");
                
                    var eve = [];
                    response.forEach(function(entry) {
                        
                        eve.push( {
                                id:entry.id,
                                referenceId:entry.referenciaId,
                                reference:entry.referencia,
                                title: entry.title,
                                start: new Date(entry.start),
                                end: new Date(entry.end),
                                backgroundColor: entry.backgroundColor, //red                                
                            });
                       
                    });
               
                     calElement.fullCalendar({
                        isRTL: isRTL,
                        header: {
                            left:   'prev,next today',
                            center: 'title',
                            right:  'month,agendaWeek,agendaDay'
                        },
                        buttonIcons: { // note the space at the beginning
                            prev:    ' fa fa-caret-left',
                            next:    ' fa fa-caret-right'
                        },
                        buttonText: {
                            today: 'Hoy',
                            month: 'Mes',
                            week:  'Semana',
                            day:   'Dia'
                        },
                        editable: true,
                        droppable: false, // this allows things to be dropped onto the calendar 
                        // This array is the events sources
                        events: eve,
                        eventClick: function(calEvent, jsEvent, view) {                            
                            
                                    var modalInstance = $modal.open({
                                      templateUrl: 'app/views/agenda/agendaCalendario_form.html',
                                      controller: 'agendaCalendarioController',
                                      size: 'lg',
                                      resolve: {
                                        parameters: { id : calEvent.id,referencia:calEvent.reference}
                                      }
                                    });
                                    modalInstance.result.then(function (parameters) {
                                    });
                              }
                    });       
               $rootScope.loadingVisible = false;

            }, function(faild) {
                $rootScope.loadingVisible = false;
                console.log(faild.Message);
                //message.show("error", faild.Message);
            });            
           
      }    
    
     

})();