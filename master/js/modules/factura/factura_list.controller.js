/**=========================================================
 * Module: app.factura.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.factura')
    .controller('facturaListController', facturaListController);

  facturaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'facturaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function facturaListController($scope, $filter, $state, ngDialog, tpl, facturaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {	
		
	$scope.ESTADO_PROCESO = 'P';
    $scope.ESTADO_ANULADO = 'A';
    $scope.ESTADO_CERRADO= 'C';	
    $scope.ESTADO_CANCELADO= 'CA';		
        
    var estadoSeleccionado= $state.params.filters.estadoSeleccionado;

    $scope.facturas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombrefacturas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.facturas.selectedAll = !$scope.facturas.selectedAll; 
        for (var key in $scope.facturas.selectedItems) {
          $scope.facturas.selectedItems[key].check = $scope.facturas.selectedAll;
        }
      },
      add : function() {
        
       
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      tipo: 'FAC',
                      consecutivo: '',
                      asunto: '',
                      estado: '',
                      descripcion: '',                  
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.factura_add', params : { factura: oport } });
            $state.go('app.factura_add');
      
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.factura_edit', params : { factura: item } });
        $state.go('app.factura_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            facturaHttp.remove({}, { id: id }, function(response) {
                $scope.facturas.getData();
                message.show("success", "factura eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.facturas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    facturaHttp.remove({}, { id: id }, function(response) {
                        $scope.facturas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.facturas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.facturas.filterText,
          "precision": false
        }];
        $scope.facturas.selectedItems = $filter('arrayFilter')($scope.facturas.dataSource, paramFilter);
        $scope.facturas.paginations.totalItems = $scope.facturas.selectedItems.length;
        $scope.facturas.paginations.currentPage = 1;
        $scope.facturas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.facturas.paginations.currentPage == 1 ) ? 0 : ($scope.facturas.paginations.currentPage * $scope.facturas.paginations.itemsPerPage) - $scope.facturas.paginations.itemsPerPage;
        $scope.facturas.data = $scope.facturas.selectedItems.slice(firstItem , $scope.facturas.paginations.currentPage * $scope.facturas.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.facturas.data = [];
        $scope.facturas.loading = true;
        $scope.facturas.noData = false;
        var parametros= {
            "proceso":'FAC',
            "estado":$scope.estado.current.value
        };
       
        facturaHttp.getList({}, parametros,function(response) {
          $scope.facturas.selectedItems = response;
          $scope.facturas.dataSource = response;
          for(var i=0; i<$scope.facturas.dataSource.length; i++){
            $scope.facturas.nombrefacturas.push({id: i, nombre: $scope.facturas.dataSource[i]});
          }
          $scope.facturas.paginations.totalItems = $scope.facturas.selectedItems.length;
          $scope.facturas.paginations.currentPage = 1;
          $scope.facturas.changePage();
          $scope.facturas.loading = false;
          ($scope.facturas.dataSource.length < 1) ? $scope.facturas.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
    //ESTADOS
    $scope.estado = {
        current : {}, data:[],
        getData : function() {            
            $scope.estado.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estado.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estado.data.push({value:'C', descripcion: 'Cerrado'}); 
            $scope.estado.data.push({value:'CA', descripcion: 'Cancelado'}); 
            $scope.estado.current =$scope.estado.data[0];
        }        
    }

	//CARGAMOS LOS ESTADOS Y LAS FACTURAS
    $scope.estado.getData();
    $scope.facturas.getData();  
        
	}
  
  
  })();