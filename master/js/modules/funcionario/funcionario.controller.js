/**=========================================================
 * Module: app.funcionario.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.funcionario')
    .controller('funcionarioController', funcionarioController);

  funcionarioController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'funcionarioHttp', 'ngDialog', 'tpl', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function funcionarioController($scope, $filter, $state, LDataSource, funcionarioHttp, ngDialog, tpl, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
      var funcionario = $state.params.funcionario;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      $scope.funcionario = {
		model : {
				  id: 0,
				  nombre: '',
				  empresaId: 0,
				  matricula: '',
				  email: '',
				  documento: '',
				  direccion: '',
				  estado: '',
				  fechaIngreso: '',
				  codigo: '',
				  telefono: '',
				  fechaAct: '',
				  usuarioAct: '',
				  fechaReg: '',
				  usuarioReg: '',
				  usuario: '',
				  contrasenia: '',
				  tipoAuth: '',
                  roles:[]
		},
        fechaIngreso : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaIngreso.isOpen = true;
        }
      },   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.funcionario.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.funcionario', params : { filters : {}, data : []} });
        $state.go('app.funcionario');
      },
      save : function() {		  
       
		  
          		if($scope.funcionario.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.funcionario.model.documento ==""){message.show("warning", "Documento requerido");return;}				
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.funcionario.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoAuth.current){message.show("warning", "Tipo autenticación requerido");return;}
				else{$scope.funcionario.model.tipoAuth= $scope.tipoAuth.current.value;}
				
				if($scope.funcionario.fechaIngreso.value==""){message.show("warning", "Fecha de ingreso requerido");return;}
				else{$scope.funcionario.model.fechaIngreso=Date.parse(new Date($scope.funcionario.fechaIngreso.value));}				
			
				$scope.funcionario.model.empresaId=1;

			
          
			//INSERTAR
            if($scope.funcionario.model.id==0){
				
				$rootScope.loadingVisible = true;
				funcionarioHttp.save({}, $scope.funcionario.model, function (data) { 
					
						funcionarioHttp.read({},{ id : data.id}, function (data) {
							$scope.funcionario.model=data;
							funcionario.id=$scope.funcionario.model.id; 
							if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
							if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));} 		
								
							message.show("success", "Funcionario creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.funcionario.model.id){
					$state.go('app.funcionario');
				}else{
					
					funcionarioHttp.update({}, $scope.funcionario.model, function (data) {
					  $rootScope.loadingVisible = false;
					  $scope.funcionario.model=data;
						if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
						if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));}   					  
					   message.show("success", "Funcionario actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
								
					
				   
				}
			}
		}
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }
	//TIPO AUTENTICACION
      $scope.tipoAuth = {
            current: {},
            data: [],
            getData: function() {
                $scope.tipoAuth.data.push({value: 'APP',descripcion: 'Aplicación'});
                $scope.tipoAuth.data.push({value: 'LDAP',descripcion: 'Directorio Activo'});
            }
        }
	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoAuth.getData();
	
	
	//CARGAMOS LOS DATOS DEL FUNCIONARIO
	
	
	if(funcionario.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
    }
    else{
        $rootScope.loadingVisible = true;
		
        funcionarioHttp.read({},$state.params.funcionario, function (data) { 
            $scope.funcionario.model = data;
			
            if($scope.funcionario.model.fechaIngreso){$scope.funcionario.fechaIngreso.value=new Date(parseFloat($scope.funcionario.model.fechaIngreso));}    
			if($scope.funcionario.model.fechaReg){$scope.funcionario.fechaReg.value=new Date(parseFloat($scope.funcionario.model.fechaReg));}    
			if($scope.funcionario.model.fechaAct){$scope.funcionario.fechaAct.value=new Date(parseFloat($scope.funcionario.model.fechaAct));}   
            
			//$scope.estados.current=$scope.funcionario.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.funcionario.model.estado})[0];
            $scope.tipoAuth.current = $filter('filter')($scope.tipoAuth.data, {value : $scope.funcionario.model.tipoAuth})[0];
			
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
	
    
    
  }
})();