/**=========================================================
 * Module: app.forma30.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.seguimiento')
    .controller('seguimientoController', seguimientoController);

  seguimientoController.$inject = ['$scope', '$rootScope', '$state', '$modalInstance', 'seguimientoHttp', 'parameters',  'Upload', 'base64', 'END_POINT', 'SERVER_URL', 'tokenManager', 'message', '$window'];


  function seguimientoController($scope, $rootScope, $state, $modalInstance, seguimientoHttp, parameters, Upload, base64, END_POINT, SERVER_URL, tokenManager, message, $window) {


    $scope.seguimiento = {
      model : {     
        id:0,  
        idReferencia : '',
        tipoReferencia : '',
        descripcion : '',
        usuarioReg : '',
        fechaReg : ''
          
      },
      paginations : {
        maxSize : 3,
        itemsPerPage : 3,
        currentPage : 0,
        totalItems : 0
      },
      filterText : '',
      dataSource : [],
      selectedItems : [],
      data : [],
      noData : true,
      loading : false,
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.seguimiento.filterText,
          "precision": false
        }];
        $scope.fileselectedItems = $filter('arrayFilter')($scope.seguimiento.dataSource, paramFilter);
        $scope.seguimiento.paginations.totalItems = $scope.seguimiento.selectedItems.length;
        $scope.seguimiento.paginations.currentPage = 1;
        $scope.seguimiento.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.seguimiento.paginations.currentPage == 1 ) ? 0 : ($scope.seguimiento.paginations.currentPage * $scope.seguimiento.paginations.itemsPerPage) - $scope.seguimiento.paginations.itemsPerPage;
        $scope.seguimiento.data = $scope.seguimiento.selectedItems.slice(firstItem , $scope.seguimiento.paginations.currentPage * $scope.seguimiento.paginations.itemsPerPage);
      },
      refresh : function() {
        var params = {
          idReferencia : $scope.seguimiento.model.idReferencia,
          tipoReferencia : $scope.seguimiento.model.tipoReferencia
        };
        $scope.seguimiento.getData(params);
      },
      setData : function(data) {
        $scope.seguimiento.selectedItems = data;
        $scope.seguimiento.dataSource = data;
        $scope.seguimiento.paginations.totalItems = $scope.seguimiento.selectedItems.length;
        $scope.seguimiento.paginations.currentPage = 1;
        $scope.seguimiento.changePage();
        $scope.seguimiento.noData = false;
        $scope.seguimiento.loading = false;
        ($scope.seguimiento.dataSource.length < 1) ? $scope.seguimiento.noData = true : null;
      },
      getData : function(params) {
        $scope.seguimiento.data = [];
        $scope.seguimiento.loading = true;
        $scope.seguimiento.noData = false;

        seguimientoHttp.getSeguimientos({}, params, function(response) {
          $scope.seguimiento.setData(response);
        })
      },
      visualizar : function(id) {
        var params = {
          id : id 
        };

        seguimientoHttp.getSeguimiento({}, params, function(response) {
          if (response) {
              $scope.seguimiento.model.descripcion=response.descripcion;
              
          }
        })
      },
      eliminar : function(id) {
        var params = {
          id : id 
        };

        $rootScope.loadingVisible = true;
        seguimientoHttp.removeSeguimiento(params, function(response) {
          $scope.seguimiento.refresh();
          message.show("info", "Seguimiento eliminado correctamente");
          $rootScope.loadingVisible = false;
        }, function() {
          message.show("error", "Ocurrio un error al intentar eliminar el adjunto");
          $rootScope.loadingVisible = false;
        });
      },     
      close : function() {
        $modalInstance.dismiss('cancel');
      },        
      save : function() {	
          
        $rootScope.loadingVisible = true;
        seguimientoHttp.addSeguimiento({}, $scope.seguimiento.model, function (data) { 

             message.show("success", "Seguimiento creaado correctamente");
             $rootScope.loadingVisible = false;
             $scope.seguimiento.refresh();

        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      }
    }
    
    $scope.hideDelete=true;
    
    $scope.seguimiento.model.idReferencia = parameters.id.toString();
    $scope.seguimiento.model.tipoReferencia = parameters.referencia.toString();
    //$scope.hideDelete = (parameters.hideDelete) ? parameters.hideDelete : false;
    $scope.seguimiento.refresh();

  }

})();