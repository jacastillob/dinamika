/**=========================================================
 * Module: app.consecutivo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .controller('consecutivoController', consecutivoController);

  consecutivoController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'consecutivoHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function consecutivoController($scope, $filter, $state, LDataSource, consecutivoHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var consecutivo = $state.params.consecutivo;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.consecutivo = {
		model : {
              id: 0,
              terceroId: 0,
              contactoId: 0,   
              funcionarioId: 0,   
              tipo: 0,
              codigo: '',			  
			  estado: '',
			  descripcion: '',
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''	
          
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.consecutivo.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.consecutivo.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.consecutivo', params : { filters : {procesoSeleccionado:consecutivo.tipo}, data : []} });
        $state.go('app.consecutivo');
          
      },
      save : function() {		  
       
		  
                
                if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
                }else{$scope.consecutivo.model.terceroId = $scope.tercerosOp.current.id;}
          
          		if($scope.consecutivo.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			
				
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.consecutivo.model.funcionarioId = $scope.funcionarios.current.id;}
          
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.consecutivo.model.estado= $scope.estados.current.value;}
				
				if(!$scope.tipoConsecutivo.current){message.show("warning", "Tipo consecutivo");return;}
				else{$scope.consecutivo.model.tipo= $scope.tipoConsecutivo.current.id;}	
   	
          
			//INSERTAR
            if($scope.consecutivo.model.id==0){
				
				$rootScope.loadingVisible = true;
                
				consecutivoHttp.save({}, $scope.consecutivo.model, function (data) { 
					
						consecutivoHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.consecutivo.model=data;
							consecutivo.id=$scope.consecutivo.model.id; 
							if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
							if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));} 		
								
							message.show("success", "consecutivo creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.consecutivo.model.id){
					$state.go('app.consecutivo');
				}else{
					
					consecutivoHttp.update({}, $scope.consecutivo.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.consecutivo.model=data;
						if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
						if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));}  
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                     $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : data.contactoId })[0]; 
                    message.show("success", "consecutivo actualizado satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/consecutivo/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.consecutivo.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.consecutivo.model.id,
                         referencia : 'CONSECUTIVO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }        
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'AN', descripcion: 'Anulado'});            
            $scope.estados.data.push({value:'AP', descripcion: 'Aprobado'}); 
        }        
    }
	//TIPO PROCESO
      $scope.tipoConsecutivo = {
            current: {},
            data: [],
            getData: function() { 
               consecutivoHttp.getTipoConsecutivos({}, {}, function(response) {
                      
                     $scope.tipoConsecutivo.data = $filter('orderBy')(response, 'id');                     
                     $scope.tipoConsecutivo.current= $filter('filter')($scope.tipoConsecutivo.data, {id : consecutivo.tipo})[0];  
                     $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });
                    
            }
        }
      

    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            consecutivoHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.consecutivo.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.consecutivo.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.consecutivo.model.terceroId=$scope.tercerosOp.current.id;
      },        
      'setContactoTercero' : function() {          
          
          $scope.consecutivo.model.terceroId=$scope.tercerosOp.current.id;
          $scope.contactosOp.getData();
      }
    }
    //CONTACTOS
    $scope.contactosOp = {
      current : {}, data:[],
      getData : function() {                  
          
               $rootScope.loadingVisible = true;
                consecutivoHttp.getContactosEmpresa({}, { terceroId: $scope.consecutivo.model.terceroId }, function(response) {   
                    
             $scope.contactosOp.data = response ;
                                                                                                                     
              if($scope.consecutivo.model ){ 
                    
                    $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.consecutivo.model.contactoId })[0];
               }
                else{
                    $scope.contactosOp.current=$scope.contactosOp.data[0];                     
                }
                                                                                                                   
                                                                                                                       
                    
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
            });
          
      },
      'setContacto' : function() {
        
          $scope.consecutivo.model.contactoId=$scope.contactosOp.current.id;
      }
    }
    
    //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            
            consecutivoHttp.getFuncionarios({}, {}, function(response) {
            
                $scope.funcionarios.data = response;  
                $scope.funcionarios.data.push({id:(-1), funcionario:"Seleccione"});
                $scope.funcionarios.data=$filter('orderBy')($scope.funcionarios.data, 'id');                
                $rootScope.loadingVisible = false;
                
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      }
    } 
    

	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
	$scope.tipoConsecutivo.getData();
    $scope.tercerosOp.getData();   
    $scope.funcionarios.getData();   
    
      
   
  
	
	//CARGAMOS LOS DATOS DEL consecutivo	
	
	if(consecutivo.id==0){        
        
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.consecutivo.model.tipo=consecutivo.tipo;  
      $scope.funcionarios.current= $scope.funcionarios.data[0];
               
      
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            consecutivoHttp.read({},$state.params.consecutivo, function (data) { 
                
            $scope.consecutivo.model = data;	
            $scope.btnAdjunto=true;          
               
			if($scope.consecutivo.model.fechaReg){$scope.consecutivo.fechaReg.value=new Date(parseFloat($scope.consecutivo.model.fechaReg));}    
			if($scope.consecutivo.model.fechaAct){$scope.consecutivo.fechaAct.value=new Date(parseFloat($scope.consecutivo.model.fechaAct));}  
            
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.consecutivo.model.terceroId })[0];  
            $scope.contactosOp.getData();               
            $scope.contactosOp.current = $filter('filter')($scope.contactosOp.data, { id : $scope.consecutivo.model.contactoId })[0]; 
                
            $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.consecutivo.model.funcionarioId })[0]; 
                
			//$scope.estados.current=$scope.consecutivo.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.consecutivo.model.estado})[0];
            $scope.tipoConsecutivo.current = $filter('filter')($scope.tipoConsecutivo.data, {id : $scope.consecutivo.model.tipoProceso})[0];
        
                          
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
    
    
  }
})();