/**=========================================================
 * Module: app.consecutivo.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.consecutivo')
    .controller('consecutivoListController', consecutivoListController);

  consecutivoListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'consecutivoHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function consecutivoListController($scope, $filter, $state, ngDialog, tpl, consecutivoHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
        
    
    var procesoSeleccionado= $state.params.filters.procesoSeleccionado;
	
		
	$scope.ESTADO_PROCESO = 'P'; 
    $scope.ESTADO_APROBADO= 'AP';	
    $scope.ESTADO_ANULADO= 'AN';	
    		

    $scope.consecutivos = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreconsecutivos:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.consecutivos.selectedAll = !$scope.consecutivos.selectedAll; 
        for (var key in $scope.consecutivos.selectedItems) {
          $scope.consecutivos.selectedItems[key].check = $scope.consecutivos.selectedAll;
        }
      },
      add : function() {
        
        if($scope.tipoConsecutivo.current.id){        
            
              var oport = {
                      id: 0,
                      terceroId: 0,
                      funcionarioId: 0,
                      tipo: $scope.tipoConsecutivo.current.id,
                      codigo: '',                      
                      estado: '',
                      descripcion: '',
                      fechaAct: '',
                      usuarioAct: '',
                      fechaReg: '',
                      usuarioReg: ''				
              };
            
            parametersOfState.set({ name : 'app.consecutivo_add', params : { consecutivo: oport } });
            $state.go('app.consecutivo_add');
          
          }
          else{
              
              message.show("warning", "Debe seleccionar el tipo de proceso!!")
          }
          
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.consecutivo_edit', params : { consecutivo: item } });
        $state.go('app.consecutivo_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            consecutivoHttp.remove({}, { id: id }, function(response) {
                $scope.consecutivos.getData();
                message.show("success", "consecutivo eliminada satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.consecutivos.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    consecutivoHttp.remove({}, { id: id }, function(response) {
                        $scope.consecutivos.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.consecutivos.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.consecutivos.filterText,
          "precision": false
        }];
        $scope.consecutivos.selectedItems = $filter('arrayFilter')($scope.consecutivos.dataSource, paramFilter);
        $scope.consecutivos.paginations.totalItems = $scope.consecutivos.selectedItems.length;
        $scope.consecutivos.paginations.currentPage = 1;
        $scope.consecutivos.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.consecutivos.paginations.currentPage == 1 ) ? 0 : ($scope.consecutivos.paginations.currentPage * $scope.consecutivos.paginations.itemsPerPage) - $scope.consecutivos.paginations.itemsPerPage;
        $scope.consecutivos.data = $scope.consecutivos.selectedItems.slice(firstItem , $scope.consecutivos.paginations.currentPage * $scope.consecutivos.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.consecutivos.data = [];
        $scope.consecutivos.loading = true;
        $scope.consecutivos.noData = false;
        var parametros= {
            "tipo":$scope.tipoConsecutivo.current.id,   
            "estado":$scope.estado.current.id   
            
        };          
        consecutivoHttp.getList({}, parametros,function(response) {
              $scope.consecutivos.selectedItems = response;
              $scope.consecutivos.dataSource = response;
            
              for(var i=0; i<$scope.consecutivos.dataSource.length; i++){
                $scope.consecutivos.nombreconsecutivos.push({id: i, nombre: $scope.consecutivos.dataSource[i]});
              }
            
              $scope.consecutivos.paginations.totalItems = $scope.consecutivos.selectedItems.length;
              $scope.consecutivos.paginations.currentPage = 1;
              $scope.consecutivos.changePage();
              $scope.consecutivos.loading = false;
              ($scope.consecutivos.dataSource.length < 1) ? $scope.consecutivos.noData = true : null;
              $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
      //CARGAMOS PROCESOS
    $scope.tipoConsecutivo = {
        current : {}, data:[],
        getData : function() {
            
            $rootScope.loadingVisible = true;
                 consecutivoHttp.getTipoConsecutivos({}, {}, function(response) {
                     $scope.tipoConsecutivo.data = $filter('orderBy')(response, 'id');
                     
                     if(procesoSeleccionado){        
      
                            $scope.tipoConsecutivo.current = $filter('filter')($scope.tipoConsecutivo.data, {id : procesoSeleccionado})[0];  
                    }
                     else{
                         $scope.tipoConsecutivo.current =$scope.tipoConsecutivo.data[0];
                     }
                     
                     $scope.consecutivos.getData();  
                    $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", faild.Message);
            });
            
        }        
    }	   
        
        //ESTADOS
    $scope.estado = {
        current : {}, data:[],
        getData : function() {
            $scope.estado.data.push({id:'P', nombre: 'Proceso'});
            $scope.estado.data.push({id:'AN', nombre: 'Anulado'});            
            $scope.estado.data.push({id:'AP', nombre: 'Aprobado'}); 
            $scope.estado.current =$scope.estado.data[0];
        }        
    }
    
	//CARGAMOS LOS TIPOS DE PROCESO
    $scope.tipoConsecutivo.getData();
    $scope.estado.getData();
    
        
   
        
	}
  
  
  })();