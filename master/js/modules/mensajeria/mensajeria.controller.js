/**=========================================================
 * Module: app.mensajeria.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.mensajeria')
    .controller('mensajeriaController', mensajeriaController);

  mensajeriaController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'mensajeriaHttp', 'ngDialog', 'tpl','$modal', 'message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function mensajeriaController($scope, $filter, $state, LDataSource, mensajeriaHttp, ngDialog, tpl,$modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
     
      var mensajeria = $state.params.mensajeria;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?¿ ]+$/;
      
      $scope.mensajeria = {
		model : {
                id: 0,
                terceroId: 0,                      
                consecutivo: '',                     
                estado: '',
                descripcion: '',                  
                fechaRec: '',
                tipo: '1',
                fechaAct: '',
                usuarioAct: '',
                fechaReg: '',
                usuarioReg: ''		
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaAct.isOpen = false;
        }
      },	
       fechaRec : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.mensajeria.fechaRec.isOpen = true;
        }
      },
      back : function() {
          parametersOfState.set({ name : 'app.mensajeria', params : { filters : {}, data : []} });
        $state.go('app.mensajeria');
          
      },
      save : function() {				  
               
            if(!$scope.tercerosOp.current){message.show("warning", "Cliente requerido");return;
            }else{$scope.mensajeria.model.terceroId = $scope.tercerosOp.current.id;}          

            if($scope.mensajeria.model.descripcion ==""){message.show("warning", "Descripción requerido");return;}			

            if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
            else{$scope.mensajeria.model.estado= $scope.estados.current.value;}		
          
            if(!$scope.jornadas.current){message.show("warning", "Jornada requerida");return;}
            else{$scope.mensajeria.model.jornada= $scope.jornadas.current.value;}	
          
            if(!$scope.tipos.current){message.show("warning", "Tipo requerido");return;}
            else{$scope.mensajeria.model.tipo= $scope.tipos.current.value;}		

            $scope.mensajeria.model.fechaRec=Date.parse(new Date($scope.mensajeria.fechaRec.value));
              
			//INSERTAR
            if($scope.mensajeria.model.id==0){
				
				$rootScope.loadingVisible = true;
				mensajeriaHttp.save({}, $scope.mensajeria.model, function (data) { 
					
						mensajeriaHttp.read({},{ id : data.id}, function (data) {
                            
							$scope.mensajeria.model=data;
							mensajeria.id=$scope.mensajeria.model.id; 
                            
							if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}  
							if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));} 	
                            if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));} 	
                            
                           
                           
                            
                            $scope.btnAdjunto=true;
                                
								
							message.show("success", "Información registrada satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.mensajeria.model.id){
					$state.go('app.mensajeria');
				}else{
					
					mensajeriaHttp.update({}, $scope.mensajeria.model, function (data) {
                        
					  $rootScope.loadingVisible = false;
					       $scope.mensajeria.model=data;
						if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}    
						if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));} 
                        if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));}                       
                        
                        
                     $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : data.terceroId })[0];   
                            message.show("success", "Información actualizada satisfactoriamente");	
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  							
					
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'PROCESO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'mensajeria' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        crearCliente: function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/mensajeria/crearCliente.html',
          controller: 'crearClienteController',
          size: 'lg',
          resolve: { parameters: {} }
        });
        modalInstance.result.then(function (parameters) {
            
            $scope.mensajeria.model.terceroId=parameters.id;
            $scope.tercerosOp.getData();
            
        });
      },agenda : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/agenda/agenda.html',
          controller: 'agendaController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.mensajeria.model.id,
                         referencia : 'mensajeria' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }        
          
      }
    
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'P', descripcion: 'Proceso'});
            $scope.estados.data.push({value:'A', descripcion: 'Anulado'});
            $scope.estados.data.push({value:'C', descripcion: 'Cerrado'});             
        }        
    }
    //TIPO
    $scope.tipos = {
        current : {}, data:[],
        getData : function() {
            $scope.tipos.data.push({value:'1', descripcion: 'Recibida'});
            $scope.tipos.data.push({value:'2', descripcion: 'Programada'});                    
        }        
    }
    //JORNADAS
    $scope.jornadas = {
        current : {}, data:[],
        getData : function() {
            $scope.jornadas.data.push({value:'M', descripcion: 'Mañana'});
            $scope.jornadas.data.push({value:'T', descripcion: 'Tarde'});                         
        }        
    }
    
    //TERCEROS
    $scope.tercerosOp = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            mensajeriaHttp.getTerceros({}, {}, function(response) {                
            $scope.tercerosOp.data = response ;              
                
               if($scope.mensajeria.model){
                    
                    $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.mensajeria.model.terceroId })[0];                   
               }
                else{
                    $scope.tercerosOp.current=$scope.tercerosOp.data[0];                     
                }
               
                
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setTercero' : function() {
          $scope.mensajeria.model.terceroId=$scope.tercerosOp.current.id;
      }
    }

	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();
    $scope.jornadas.getData();
    $scope.tipos.getData();	  
    $scope.tercerosOp.getData();     
      
   
  
	
	//CARGAMOS LOS DATOS DEL mensajeria	
	
	if(mensajeria.id==0){
        
      $scope.estados.current=$scope.estados.data[0]; 
      $scope.jornadas.current=$scope.jornadas.data[0]; 
      $scope.tipos.current=$scope.tipos.data[0]; 
      var date = new Date(); 
      $scope.mensajeria.fechaRec.value=new Date(date.getFullYear(), date.getMonth(), date.getDate());
          
        
    }
    else{   
        $rootScope.loadingVisible = true;
		
            mensajeriaHttp.read({},$state.params.mensajeria, function (data) { 
                
            $scope.mensajeria.model = data;	
            $scope.btnAdjunto=true;            
               
			if($scope.mensajeria.model.fechaReg){$scope.mensajeria.fechaReg.value=new Date(parseFloat($scope.mensajeria.model.fechaReg));}    
			if($scope.mensajeria.model.fechaAct){$scope.mensajeria.fechaAct.value=new Date(parseFloat($scope.mensajeria.model.fechaAct));}  
            if($scope.mensajeria.model.fechaRec){$scope.mensajeria.fechaRec.value=new Date(parseFloat($scope.mensajeria.model.fechaRec));}  
            $scope.tercerosOp.current = $filter('filter')($scope.tercerosOp.data, { id : $scope.mensajeria.model.terceroId })[0]; 
			//$scope.estados.current=$scope.mensajeria.model.estado;	            
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.mensajeria.model.estado})[0];
            $scope.jornadas.current = $filter('filter')($scope.jornadas.data, {value : $scope.mensajeria.model.jornada})[0];    
            $scope.tipos.current = $filter('filter')($scope.tipos.data, {value : $scope.mensajeria.model.tipo})[0];          
                    
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
      

      
        
	
    
    
  }
})();