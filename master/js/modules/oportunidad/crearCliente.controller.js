/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.oportunidad')
    .controller('crearClienteController', crearClienteController);

  crearClienteController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'oportunidadHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function crearClienteController($scope, $filter, $state, $modalInstance, LDataSource, oportunidadHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.cliente = {     
          model:{
              identificacion: '',
              nombre: '',
              telefono: '', 
              direccion: '', 
              email: '',              
              nombreContacto: '',
              telefonoContacto: '',
              cargoContacto: ''
          },         
          
         save:  function(){ 
                
                if($scope.cliente.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                
                   
             var _http = new oportunidadHttp($scope.cliente.model);
                  _http.$addTercero(function(response){                  
                      
                      
                      message.show("success", "Cliente creado satisfactoriamente!!");
                      $modalInstance.close(response);
                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
                 
              
              
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }      
       	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      //$scope.cliente.model=parameters.cliente;
  }
})();