/**=========================================================
 * Module: app.setMenu
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.setMenu')
    .controller('setMenuController', setMenuController);

  setMenuController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'tpl', 'ngDialog', 'parametersOfState', 'REGULAR_EXPRESION', 'menuRolHttp', 'message', '$rootScope'];


  function setMenuController($scope, $filter, $state, LDataSource, $modal, tpl, ngDialog, parametersOfState, REGULAR_EXPRESION, menuRolHttp, message, $rootScope){
    $scope.menuName='';
    $scope.menuId=0;
    $scope.nameMenu='';
    $scope.menu= {
        current : {
          menuName:'',
          menuId:0
        },
        data:[],
        getData: function(){
            $scope.menuName='';
            $scope.menuId=0;
            $scope.roles.data=[];
            $rootScope.loadingVisible = true;
            menuRolHttp.getListMenu({},{},function(response) {
                $scope.menu.data = $filter('orderBy')(response, 'menuId');
                for(var i = 0; i < $scope.menu.data.length; i++){
                    $scope.menu.data[i].submenu = $filter('orderBy')($scope.menu.data[i].submenu, 'menuId');
                    $scope.menu.data[i].menuName= $scope.menu.data[i].label;
                    $scope.menu.data[i].show=false;
                    if($scope.menu.data[i].submenu!=null){
                        for(var j=0; j<$scope.menu.data[i].submenu.length; j++){
                            $scope.menu.data[i].submenu[j].menuName=$scope.menu.data[i].menuName + "/" + $scope.menu.data[i].submenu[j].label;
                            $scope.menu.data[i].submenu[j].show=false;
                            if($scope.menu.data[i].submenu[j].submenu!=null){
                                $scope.menu.data[i].submenu[j].submenu = $filter('orderBy')($scope.menu.data[i].submenu[j].submenu, 'menuId');
                                for(var k=0; k<$scope.menu.data[i].submenu[j].submenu.length; k++){
                                    $scope.menu.data[i].submenu[j].submenu[k].show=false;
                                    $scope.menu.data[i].submenu[j].submenu[k].menuName=$scope.menu.data[i].label + "/" + $scope.menu.data[i].submenu[j].label + "/" + $scope.menu.data[i].submenu[j].submenu[k].label;
                                }
                            }
                        }
                    }
                }
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $scope.menu.data = [];
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los Menus: " + faild.Message);
            })
        }, edit: function (item){
            var parametros={
              detalle:{},
              edit:true,
              menuName:'Menú'
            };
            parametros.detalle=item;
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
        }, addSubmenu: function (item){
            var parametros={
              detalle:{
                alert: '',
                icon: '',
                index: 0,
                label: '',
                orden: 0,
                padreId: item.menuId,
                param: '',
                sref: '',
                submenu: null,
                text: '',
                urlParam: ''
              },
              edit:false,
              menuName:'Submenú'
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
            
        }, addMenu: function (){
            var parametros={
              detalle:{
                alert: '',
                icon: '',
                index: 0,
                label: '',
                orden: 0,
                padreId: 0,
                param: '',
                sref: '',
                submenu: null,
                text: '',
                urlParam: ''
              },
              edit:false,
              menuName:'Menú',
            };
            var parameter = parametros;
            var modalInstance = $modal.open({
                templateUrl: 'app/views/menu/menu_add.html',
                controller: 'menuAddController',
                size: 'md',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {$scope.menu.getData();}, function (parameters) {$scope.menu.getData();});
            
        }, getRol: function(item){
            $scope.menuName=item.menuName;
            $scope.menuId=item.menuId;
            $scope.roles.getData();
            $scope.nameMenu=item.label;
        }, remove: function(item){
            ngDialog.openConfirm({
                template: tpl.path,
                className: 'ngdialog-theme-default',
                scope: $scope
            }).then(function (value) {
                var parametros={menuId: $scope.roles.menuId, rol: 'ADMINISTRADOR', rolId: 1, seleccionado: false};
                var _Http = new menuRolHttp(parametros);
                _Http.$unassignRol(function(response) {
                  $rootScope.loadingVisible = false;
                }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", "No se Desasignó el Rol: " + faild.Message);
                })
                var id = item.menuId;
                console.log(id);
                $rootScope.loadingVisible = true;
                menuRolHttp.removeMenu({}, { id: id }, function(response) {
                    $scope.menu.getData();
                    $rootScope.loadingVisible = false;
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", "No se Eliminó el Menú: " + faild.Message);
                });
            });
        }
    }
    $scope.roles= {
        current : {},
        data:[],
        menuId: 0,
        getData: function(){
            $rootScope.loadingVisible = true;
            menuRolHttp.getMenuRol({},{ id : $scope.menuId}, function (response) {
                $scope.roles.data = response;
                $scope.roles.menuId = $scope.menuId;
                $rootScope.loadingVisible = false;
            }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        },
        checkRol: function(item){
            var _Http = new menuRolHttp(item);
            $rootScope.loadingVisible = true;
            if(item.seleccionado==true){
              $rootScope.loadingVisible = true;
              _Http.$assignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Asignó el Rol: " + faild.Message);
              })
            }
            else{
              _Http.$unassignRol(function(response) {
                $rootScope.loadingVisible = false;
              }, function(faild) {
                $rootScope.loadingVisible = false;
                message.show("error", "No se Desasignó el Rol: " + faild.Message);
              })
            }
        }
    }
    
    $scope.menu.getData();
  }
})();