/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroListController', terceroListController);

  terceroListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'terceroHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function terceroListController($scope, $filter, $state, ngDialog, tpl, terceroHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {
		
	$scope.ESTADO_INACTIVO = 'I';
    $scope.ESTADO_ACTIVO = 'A';
    $scope.ESTADO_ELIMINADO = 'D';
	//$rootScope.loadingVisible = true;

    var estadoSeleccionado= $state.params.filters.estadoSeleccionado;
    $scope.terceros = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreterceros:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.terceros.selectedAll = !$scope.terceros.selectedAll; 
        for (var key in $scope.terceros.selectedItems) {
          $scope.terceros.selectedItems[key].check = $scope.terceros.selectedAll;
        }
      },
      add : function() {
          var terc = {
              id: 0,
              identificacion: '', 
              nombre: '', 
			  telefono: '',
			  direccion: '',
			  correo: '',
              tipoId: 0,
			  estado: '',			  
			  fechaAct: '',
			  usuarioAct: '',
			  fechaReg: '',
			  usuarioReg: ''			 
          };
        parametersOfState.set({ name : 'app.tercero_add', params : { tercero: terc } });
        $state.go('app.tercero_add');
      },
      edit : function(item) {
        parametersOfState.set({ name : 'app.tercero_edit', params : { tercero: item } });
        $state.go('app.tercero_edit');
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            terceroHttp.remove({}, { id: id }, function(response) {
                $scope.terceros.getData();
                message.show("success", "Tercero eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.terceros.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    terceroHttp.remove({}, { id: id }, function(response) {
                        $scope.terceros.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.terceros.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.terceros.filterText,
          "precision": false
        }];
        $scope.terceros.selectedItems = $filter('arrayFilter')($scope.terceros.dataSource, paramFilter);
        $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
        $scope.terceros.paginations.currentPage = 1;
        $scope.terceros.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.terceros.paginations.currentPage == 1 ) ? 0 : ($scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage) - $scope.terceros.paginations.itemsPerPage;
        $scope.terceros.data = $scope.terceros.selectedItems.slice(firstItem , $scope.terceros.paginations.currentPage * $scope.terceros.paginations.itemsPerPage);
      },
      getData : function() {
        $scope.terceros.data = [];
        $scope.terceros.loading = true;
        $scope.terceros.noData = false;
          
        var parametros= {
            "estado":$scope.estadoTercero.current.value                
        };
          
        terceroHttp.getList({}, parametros,function(response) {
          $scope.terceros.selectedItems = response;
          $scope.terceros.dataSource = response;
          for(var i=0; i<$scope.terceros.dataSource.length; i++){
            $scope.terceros.nombreterceros.push({id: i, nombre: $scope.terceros.dataSource[i]});
          }
          $scope.terceros.paginations.totalItems = $scope.terceros.selectedItems.length;
          $scope.terceros.paginations.currentPage = 1;
          $scope.terceros.changePage();
          $scope.terceros.loading = false;
          ($scope.terceros.dataSource.length < 1) ? $scope.terceros.noData = true : null;
          $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
    }
        //CARGAMOS PROCESOS
    $scope.estadoTercero = {
        current : {}, data:[],
        getData : function() {
            $scope.estadoTercero.data.push({value:'A', descripcion: 'Activos'});
            $scope.estadoTercero.data.push({value:'I', descripcion: 'Inactivos'});
            $scope.estadoTercero.data.push({value:'D', descripcion: 'Eliminados'});
            $scope.estadoTercero.current =$scope.estadoTercero.data[0];
        }        
    }
	//CARGAMOS LOS LISTADOS
    
    $scope.estadoTercero.getData();
    $scope.terceros.getData();
        
         if(estadoSeleccionado){

            $scope.estadoTercero.current = $filter('filter')($scope.estadoTercero.data, {value : estadoSeleccionado})[0];        
            $scope.terceros.getData();        
        }
        
        
	}
  
  
  })();