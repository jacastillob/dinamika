/**=========================================================
 * Module: app.tercero.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('terceroController', terceroController);

  terceroController.$inject = ['$scope', '$filter', '$state', 'LDataSource', 'terceroHttp', 'ngDialog', 'tpl', '$modal','message', 'parametersOfState', '$q', '$rootScope', 'REGULAR_EXPRESION'];


  function terceroController($scope, $filter, $state, LDataSource, terceroHttp, ngDialog, tpl, $modal, message, parametersOfState, $q, $rootScope, REGULAR_EXPRESION) {
      
      var tercero = $state.params.tercero;
      var currentDate = new Date();
      //$scope.nombre = /^[a-zA-Z0-9áéíóú&()ÁÉÍÓÚ/\-#@!?.¿ ]+$/;
      $scope.tercero = {
		model : {
				  id: 0,
                  identificacion: '', 
                  nombre: '',                    
                  telefono: '',
                  direccion: '',
                  correo: '',
                  tipoId: 0,
                  estado: '',			  
                  fechaAct: '',
                  usuarioAct: '',
                  fechaReg: '',
                  usuarioReg: ''
		},   
		fechaReg : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.tercero.fechaReg.isOpen = false;
        }
      },   
		fechaAct : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.tercero.fechaAct.isOpen = false;
        }
      },	  
      back : function() {
          parametersOfState.set({ name : 'app.tercero', params : { filters : {estadoSeleccionado:tercero.estado}, data : []} });
        $state.go('app.tercero');
      },
      save : function() {		  
       
		          
          		if($scope.tercero.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
				if($scope.tercero.model.identificacion ==""){message.show("warning", "Identificación requerido");return;}	
                //if($scope.tercero.model.telefono ==""){message.show("warning", "Teléfono requerido");return;}	
                //if($scope.tercero.model.direccion ==""){message.show("warning", "Dirección requerido");return;}	
				
				if(!$scope.estados.current){message.show("warning", "Estado requerido");return;}
				else{$scope.tercero.model.estado= $scope.estados.current.value;}			
				
			
          
			//INSERTAR
            if($scope.tercero.model.id==0){
				
				$rootScope.loadingVisible = true;
				terceroHttp.save({}, $scope.tercero.model, function (data) { 
					
						terceroHttp.read({},{ id : data.id}, function (data) {
							$scope.tercero.model=data;
							tercero.id=$scope.tercero.model.id; 
							if($scope.tercero.model.fechaReg){$scope.tercero.fechaReg.value=new Date(parseFloat($scope.tercero.model.fechaReg));}    
							if($scope.tercero.model.fechaAct){$scope.tercero.fechaAct.value=new Date(parseFloat($scope.tercero.model.fechaAct));} 		
								
							message.show("success", "Tercero creado satisfactoriamente!!");						
							$rootScope.loadingVisible = false;
						}, function(faild) {
							$rootScope.loadingVisible = false;
							message.show("error", faild.Message);
						});
						
				}, function(faild) {
					$rootScope.loadingVisible = false;
					message.show("error", faild.Message);
				});
				
			}
			//ACTUALIZAR
			else{
				if(!$scope.tercero.model.id){
					$state.go('app.tercero');
				}else{					 
					terceroHttp.update({}, $scope.tercero.model, function (data) {
					  $rootScope.loadingVisible = false;
					  $scope.tercero.model=data;
						if($scope.tercero.model.fechaReg){$scope.tercero.fechaReg.value=new Date(parseFloat($scope.tercero.model.fechaReg));}    
						if($scope.tercero.model.fechaAct){$scope.tercero.fechaAct.value=new Date(parseFloat($scope.tercero.model.fechaAct));}   					  
					   message.show("success", "Tercero actualizado satisfactoriamente");							
					  
					  
					}, function(faild) {
					  message.show("error", faild.Message);
					  $rootScope.loadingVisible = false;
					});  
				   
				}
			}
		},
        uploadFile : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/fileManager/fileManager.html',
          controller: 'fileManagerController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.tercero.model.id,
                         referencia : 'TERCERO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      },
        seguimiento : function() {            
        var modalInstance = $modal.open({
          templateUrl: 'app/views/seguimiento/seguimiento.html',
          controller: 'seguimientoController',
          size: 'lg',
          resolve: {
            parameters: { id : $scope.tercero.model.id,
                         referencia : 'TERCERO' }
          }
        });
        modalInstance.result.then(function (parameters) {
        });
      }
    }
      
    $scope.contactoTercero = {
    model : {  
            id: 0,
            nombre: '',
            telefono: '',
			ext: '',
			email: ''
		},
      current : {},
      data:[],
      getData : function() {
          if(tercero){
              
            if($scope.tercero.model.id!=0){
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              terceroHttp.getContactosTercero({}, { terceroId: $scope.tercero.model.id }, function(response) {
                  $scope.contactoTercero.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
             
            }
          }
      },
       add : function() {          
           
           
        var parameter = {
              contactoTercero : {              
                      id: 0,
                      terceroId:$scope.tercero.model.id,
                      nombre: '',
                      telefono: '',
                      ext: '',
                      email: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {}, function (parameters) { 
            
            $scope.contactoTercero.getData();
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          terceroHttp.deleteContactoTercero({}, {contactoTerceroId: item.id}, function(response){
            $scope.contactoTercero.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
        var parameter = {
          contactoTercero : item
        };
        
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
             
            parameters: function () { return parameter; }
          }
        });        
        modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.contactoTercero.getData();});
      }
    }
    //TAB
     $scope.showTabs= function(){
        if ($scope.tercero.id==0)
          return false;
        else
          return true;
    }
    //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }	
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	
	
	//CARGAMOS LOS DATOS DEL TERCERO
	
	if(tercero.id==0){
      $scope.estados.current=$scope.estados.data[0]; 
    }
    else{        
        $rootScope.loadingVisible = true;
		$scope.divContactoTercero=true;
        terceroHttp.read({},$state.params.tercero, function (data) { 
            $scope.tercero.model = data;			
             
			if($scope.tercero.model.fechaReg){$scope.tercero.fechaReg.value=new Date(parseFloat($scope.tercero.model.fechaReg));}    
			if($scope.tercero.model.fechaAct){$scope.tercero.fechaAct.value=new Date(parseFloat($scope.tercero.model.fechaAct));}   
            
			       
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.tercero.model.estado})[0];            
			$scope.contactoTercero.getData();
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });   
    }
	
    
    
  }
})();