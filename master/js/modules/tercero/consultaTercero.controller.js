/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('consultaTerceroController', consultaTerceroController);

  consultaTerceroController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'terceroHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION'];


  function consultaTerceroController($scope, $filter, $state, $modalInstance, LDataSource, terceroHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION) {      
    
      
      $scope.consultaTercero = {     
          model:{
                  id: 0,
                  identificacion: '', 
                  nombre: '', 
                  telefono: '',
                  direccion: '',
                  correo: '',
                  tipoId: 0,
                  estado: '',			  
                  fechaAct: '',
                  usuarioAct: '',
                  fechaReg: '',
                  usuarioReg: ''
          },	
          fechaReg : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.tercero.fechaReg.isOpen = false;
            }
          },   
            fechaAct : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.tercero.fechaAct.isOpen = false;
            }
          },
         save:  function(){                    
             
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.participanteContrato.model.funcionarioId = $scope.funcionarios.current.id;}
          
                if(!$scope.cargo.current){message.show("warning", "Cargo requerido");return;}
                else{$scope.participanteContrato.model.cargoId= $scope.cargo.current.codigo;}	

             var _http = new terceroHttp($scope.participanteContrato.model);
             if(  $scope.participanteContrato.model.id==0){
                 
                  _http.$addParticipanteContrato(function(response){                  
                      message.show("success", "Participante creado satisfactoriamente!!");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editParticipanteContrato(function(response){                  
                      message.show("success", "Participante actualizado satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
              $modalInstance.close();
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      
      
    
      //ESTADOS
    $scope.estados = {
        current : {}, data:[],
        getData : function() {
            $scope.estados.data.push({value:'I', descripcion: 'Inactivo'});
            $scope.estados.data.push({value:'A', descripcion: 'Activo'});
            $scope.estados.data.push({value:'D', descripcion: 'Eliminado'});   
        }        
    }
    //CONTACTO
    $scope.contactoTercero = {
    model : {  
            id: 0,
            nombre: '',
            telefono: '',
			ext: '',
			email: ''
		},
      current : {},
      data:[],
      getData : function() {        
              
            
            //CARGAMOS EL LISTADO
            $rootScope.loadingVisible = true;
              terceroHttp.getContactosTercero({}, { terceroId: parameters.id }, function(response) {
                  $scope.contactoTercero.data = $filter('orderBy')(response, 'nombre');
                  $rootScope.loadingVisible = false;
              }, function(faild) {
                  $rootScope.loadingVisible = false;
                  message.show("error", faild.Message);
              });    
          
      },
       add : function() {          
           
           
        var parameter = {
              contactoTercero : {              
                      id: 0,
                      terceroId:$scope.tercero.model.id,
                      nombre: '',
                      telefono: '',
                      ext: '',
                      email: ''
              }            
        };

        var modalInstance = $modal.open({
          templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
        modalInstance.result.then(function (parameters) {            
            $scope.contactoTercero.getData();
        });
           
      },
      delete : function(item){
          
          $rootScope.loadingVisible = true;          
          terceroHttp.deleteContactoTercero({}, {contactoTerceroId: item.id}, function(response){
            $scope.contactoTercero.getData();
              $rootScope.loadingVisible = false;
          },function(faild) {
              $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
          });
      },
      edit : function(item) {
        var parameter = {
          contactoTercero : item
        };
        var modalInstance = $modal.open({
        templateUrl: 'app/views/tercero/contactoTercero_edit.html',
          controller: 'contactoTerceroEditController',
          size: 'lg',
          resolve: {
            parameters: function () { return parameter; }
          }
        });
          
        modalInstance.result.then(function (parameters) {   
            $scope.contactoTercero.getData();
        });
      }
    }
	//CARGAMOS LOS LISTADOS	
	$scope.estados.getData();	
    $scope.modoEdicion=true;
    $scope.modoActualizacion=false;
      
    //DEFINIMOS TITULO 
    if( parameters.referencia.toString()=="CLIENTE")
    {
        $scope.destino="Cliente";            
    }
      
      terceroHttp.read({},{ id : parameters.id}, function (data) { 
            $scope.consultaTercero.model = data;			
             
			if($scope.consultaTercero.model.fechaReg){$scope.consultaTercero.fechaReg.value=new Date(parseFloat($scope.consultaTercero.model.fechaReg));}    
			if($scope.consultaTercero.model.fechaAct){$scope.consultaTercero.fechaAct.value=new Date(parseFloat($scope.consultaTercero.model.fechaAct));}   
            
			       
            $scope.estados.current = $filter('filter')($scope.estados.data, {value : $scope.consultaTercero.model.estado})[0];            
			$scope.contactoTercero.getData();
			
           $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });  
      
    
      
  }
})();