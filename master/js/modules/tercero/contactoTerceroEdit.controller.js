/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .controller('contactoTerceroEditController', contactoTerceroEditController);

  contactoTerceroEditController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'terceroHttp',   'parameters', 'message', 'parametersOfState', 'REGULAR_EXPRESION'];


  function contactoTerceroEditController($scope, $filter, $state, $modalInstance, LDataSource, terceroHttp, parameters, message, parametersOfState, REGULAR_EXPRESION) {
      
      
      
      $scope.contactoTercero = {     
          model:{
              id: 0,
              terceroId: 0,
              nombre: '',
              telefono: '',
              ext: '',
              email: '',
              envioInf: '',
              usuario: '',
              clave: '',
              claveTmp: ''
          },         
          
         save:  function(){
             
                if($scope.contactoTercero.model.nombre ==""){message.show("warning", "Nombre requerido");return;}
                if(!$scope.envioInf.current){message.show("warning", "Envio información requerido");return;}
                else{$scope.contactoTercero.model.envioInf= $scope.envioInf.current.value;}	             
                
                 if($scope.contactoTercero.model.usuario!=""){
                     if($scope.contactoTercero.model.clave==""){
                            message.show("warning", "Debe especificar al menos una clave");return;
                        }
                     }
                else{
                    $scope.contactoTercero.model.clave='';
                    $scope.contactoTercero.model.claveTmp='';                    
                }                       
             
             var _http = new terceroHttp($scope.contactoTercero.model);
             if(  $scope.contactoTercero.model.id==0){
                 
                  _http.$addContactoTercero(function(response){                  
                      message.show("success", "Contacto creado satisfactoriamente!!");
                       $modalInstance.close();
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });             
             }
             else{         
                 
                  _http.$editContactoTercero(function(response){                  
                      message.show("success", "Contacto actualizado satisfactoriamente");
                      $modalInstance.close();                      
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
             
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      
       //ESTADOS
        $scope.envioInf = {
            current : {}, data:[],
            getData : function() {
                $scope.envioInf.data.push({value:'NA', descripcion: 'No Autorizado'}); 
                $scope.envioInf.data.push({value:'A', descripcion: 'Autorizado'});
            }        
        }	
	   //CARGAMOS LOS LISTADOS	
	   $scope.envioInf.getData();	      
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
      $scope.contactoTercero.model=parameters.contactoTercero;
      $scope.envioInf.current = $filter('filter')($scope.envioInf.data, {value :  $scope.contactoTercero.envioInf})[0];
  }
})();