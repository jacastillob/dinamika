/**=========================================================
 * Module: app.tercero.js
 * 
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.tercero')
    .service('terceroHttp', terceroHttp);

  terceroHttp.$inject = ['$resource', 'END_POINT'];


  function terceroHttp($resource, END_POINT) {

    var paramDefault = {
      id : '@id'
    };
      
    var actions = {     
      'read' : {
        'method' : 'GET',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'save':   {
        'method':'POST'
      },
      'update' : {
        'method' : 'PUT'
      },
      'remove':  {
        'method':'DELETE',
        'params' : paramDefault,
        'url' : END_POINT + '/Comercial.svc/tercero/:id'
      },
      'getList' : {
        'method' : 'GET',
        'isArray' : true,
          'params' : {
            estado : '@estado'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/estado/:estado'          
      },
      'getContactosTercero': {
          'method' : 'GET',
          'isArray' : true,
          'params' : {
            terceroId : '@terceroId'           
          },
          'url' : END_POINT + '/Comercial.svc/tercero/:terceroId/contacto'
      },
      'addContactoTercero': {
        'method':'POST',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'editContactoTercero': {
        'method':'PUT',
        'url' : END_POINT + '/Comercial.svc/tercero/contacto'
      },
      'deleteContactoTercero': {
        'method':'DELETE',
        'params' : {
            contactoTerceroId : '@contactoTerceroId'           
          },
        'url' : END_POINT + '/Comercial.svc/tercero/contacto/:contactoTerceroId'
      }
     
    };
    return $resource( END_POINT + '/Comercial.svc/tercero', {}, actions, {}); 
  }

})();