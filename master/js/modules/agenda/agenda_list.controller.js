/**=========================================================
 * Module: app.agenda.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.agenda')
    .controller('agendaListController', agendaListController);

  agendaListController.$inject = ['$scope', '$filter', '$state', 'ngDialog', 'tpl', 'agendaHttp', 'parametersOfState', 'message', '$stateParams', '$rootScope', 'REGULAR_EXPRESION'];
  
    function agendaListController($scope, $filter, $state, ngDialog, tpl, agendaHttp, parametersOfState, message, $stateParams, $rootScope, REGULAR_EXPRESION) {   
	
	
    var objFiltro= $state.params.filters.objFiltro;        
        
     var objF={              

          idTipoAgenda:0,
          fechaInicial:'',
          fechaFinal:'',
          idResponsable:0,
          idReferencia:0,
          tipoReferencia:'',
          estado:''
      }          
          
        
	$scope.ESTADO_ABIERTA = 'A';    
    $scope.ESTADO_CERRADO= 'C';	    
    $scope.ESTADO_APROBADA= 'AP';	       
        
        
    $scope.agendas = {
      paginations : {
        maxSize : 3,
        itemsPerPage : 20,
        currentPage : 0,
        totalItems : 0
      },
      procesoAsociado:'',
      fechaInicial : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
          
        },
        open : function($event) {
          $scope.agendas.fechaInicial.isOpen = true;
        }
      },
        fechaFinal : {
        isOpen : false,
        value:'',
        dateOptions : {
          formatYear: 'yy',
          startingDay: 1
        },
        open : function($event) {
          $scope.agendas.fechaFinal.isOpen = true;
        }
      },
      currentProceso: {},
      selectedAll : false,
      filterText : '',
      dataSource : [],
      nombreagendas:[],
      selectedItems : [],
      data : [],
      noData : false,
      loading : false,
      selectAll : function() {
        $scope.agendas.selectedAll = !$scope.agendas.selectedAll; 
        for (var key in $scope.agendas.selectedItems) {
          $scope.agendas.selectedItems[key].check = $scope.agendas.selectedAll;
        }
      },
      add : function() {           
          
          if($scope.procesoAsociado.current.id!=-1)
          {
              var cont = {
                    id : 0,
                    tipoId : 0,
                    referenciaId :$scope.procesoAsociado.current.id ,
                    tipoReferencia : $scope.procesoAgenda.current.proceso,
                    asunto : '',
                    descripcion : '',
                    fechaInicial: '',
                    fechaFinal: '',
                    estado: '',          
                    fechaAct: '',
                    usuarioAct: '',
                    fechaReg: '',
                    usuarioReg: '',
                    responsables : [],
                    tipoAgenda : $scope.tipoAgenda.data,
                    participantes : $scope.responsableProceso.data,
                    consecutivos : []
              };
            
              cont.objFiltro=objF;
            parametersOfState.set({ name : 'app.agenda_add', 
               params : { agenda: cont } 
                });
            $state.go('app.agenda_add'); 
              }
          else            
          {
                 message.show("warning", "Debe seleccionar un proceso asociado");
          }
      },
      edit : function(item) {
          
          item.objFiltro=objF;
          item.tipoAgenda = $scope.tipoAgenda.data;
          item.participantes = [];
          item.consecutivos = [];
            parametersOfState.set({ name : 'app.agenda_edit', params : { agenda: item } });
            $state.go('app.agenda_edit');            
      },
      removeItem : function(item) {
        ngDialog.openConfirm({
          template: tpl.path,
          className: 'ngdialog-theme-default',
          scope: $scope
        }).then(function (value) {
            
            var id = item.id;
            agendaHttp.remove({}, { id: id }, function(response) {
                $scope.agendas.getData();
                message.show("success", "agenda eliminado satisfactoriamente");
            }, function(faild) {
                message.show("error", faild.Message);
            });
        });
      },
      remove : function() {
          var removeElements= $filter('filter')($scope.agendas.selectedItems, {check : true});
          ngDialog.openConfirm({
              template: tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
          }).then(function (value) {
                for(var i=0; i<removeElements.length; i++){
                    var id = removeElements[i].id;
                    agendaHttp.remove({}, { id: id }, function(response) {
                        $scope.agendas.getData();
                    }, function(faild) {
                        message.show("error", faild.Message);
                    });    
                }
              $scope.agendas.getData();
          });
      },
      filter : function() {
        var paramFilter = [{
          "key": "$",
          "value": $scope.agendas.filterText,
          "precision": false
        }];
        $scope.agendas.selectedItems = $filter('arrayFilter')($scope.agendas.dataSource, paramFilter);
        $scope.agendas.paginations.totalItems = $scope.agendas.selectedItems.length;
        $scope.agendas.paginations.currentPage = 1;
        $scope.agendas.changePage();
      },
      changePage : function() {
        var firstItem = ($scope.agendas.paginations.currentPage == 1 ) ? 0 : ($scope.agendas.paginations.currentPage * $scope.agendas.paginations.itemsPerPage) - $scope.agendas.paginations.itemsPerPage;
        $scope.agendas.data = $scope.agendas.selectedItems.slice(firstItem , $scope.agendas.paginations.currentPage * $scope.agendas.paginations.itemsPerPage);
      },
      getData : function() {
          
       
          
            if($scope.agendas.fechaInicial.value==""){message.show("warning", "Fecha de inicial requerida");return;}
				else{objF.fechaInicial=Date.parse(new Date($scope.agendas.fechaInicial.value));}	
          
            if($scope.agendas.fechaFinal.value==""){message.show("warning", "Fecha de final requerida");return;}
				else{objF.fechaFinal=Date.parse(new Date($scope.agendas.fechaFinal.value));}	  
          
              
              objF.tipoReferencia=$scope.procesoAgenda.current.proceso;
              objF.idResponsable=$scope.responsableProceso.current.id;
              objF.estado=$scope.estadoAgenda.current.value;
              objF.idReferencia=$scope.procesoAsociado.current.id;              
              objF.idTipoAgenda=$scope.tipoAgenda.current.codigo;
       
          
            $scope.agendas.data = [];
            $scope.agendas.loading = true;
            $scope.agendas.noData = false;             
          
         agendaHttp.getListaAgendas( {},objF,function(response) {
             
              $scope.agendas.selectedItems = response;
              $scope.agendas.dataSource = response;
              for(var i=0; i<$scope.agendas.dataSource.length; i++){
                $scope.agendas.nombreagendas.push({id: i, nombre: $scope.agendas.dataSource[i]});
              }
              $scope.agendas.paginations.totalItems = $scope.agendas.selectedItems.length;
              $scope.agendas.paginations.currentPage = 1;
              $scope.agendas.changePage();
              $scope.agendas.loading = false;
              ($scope.agendas.dataSource.length < 1) ? $scope.agendas.noData = true : null;
              $rootScope.loadingVisible = false;
        }, function(faild) {
            message.show("error", faild.Message);
            $rootScope.loadingVisible = false;
        });
      }
        
    }  
    
      //CARGAMOS PROCESOS
    $scope.procesoAgenda = {
      current : {},
      data:[],
      getData : function() {
        $rootScope.loadingVisible = true;
        agendaHttp.getProcesosAgenda({}, { }, function(response) { 

        $scope.procesoAgenda.data = $filter('orderBy')(response, 'proceso');  
        if(objFiltro){

            $scope.procesoAgenda.current=  $filter('filter')($scope.procesoAgenda.data, {proceso : objFiltro.tipoReferencia})[0]; 
            $scope.procesoAgenda.setProceso();  
        }
        else{
            $scope.procesoAgenda.current =$scope.procesoAgenda.data[0];
            $scope.procesoAgenda.setProceso();  
        }           
        $rootScope.loadingVisible = false;
            
            
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
         'setProceso' : function() {
                 
                  
                  $scope.tipoAgenda.getData();
                  $scope.procesoAsociado.getData();    

                  if($scope.procesoAgenda.current.proceso=='CONTRATO')
                        $scope.agendas.procesoAsociado='Contratos:';   
           
      }
    }
    //CARGAMOS TIPO AGENDAMIENTOS POR PROCESO
    $scope.tipoAgenda = {
        current : {},
        data:[],
        getData : function() {
            var pAgen="";
            
            
            if(objFiltro){                
                pAgen=objFiltro.tipoReferencia;                
            }
            else{                
                pAgen=$scope.procesoAgenda.current.proceso;                
            }            
            
            
            if(pAgen!=""){
                
                $rootScope.loadingVisible = true;
                agendaHttp.getTipoAgenda({}, {tipoAgenda:pAgen }, function(response) {
                    
                    
                    $scope.tipoAgenda.data = $filter('orderBy')(response, 'codigo'); 
                    $scope.tipoAgenda.data.push({codigo:(-1), descripcion:"Todos"});
                    $scope.tipoAgenda.data=$filter('orderBy')($scope.tipoAgenda.data, 'codigo');
                    
                    if(objFiltro){
                
                        $scope.tipoAgenda.current=  $filter('filter')($scope.tipoAgenda.data, {codigo : objFiltro.idTipoAgenda})[0]; 
                        
                    }
                    else{

                        $scope.tipoAgenda.current = $scope.tipoAgenda.data[0];
                    }                     
                    
                    $rootScope.loadingVisible = false;

                }, function(faild) {
                    $rootScope.loadingVisible = false;
                    message.show("error", faild.Message);
                });
            }  
        }
    }
    //ESTADOS
    $scope.estadoAgenda = {
        current : {}, data:[],
        getData : function() {
            $scope.estadoAgenda.data.push({value:'', descripcion: 'Todos'});
            $scope.estadoAgenda.data.push({value:'A', descripcion: 'Abierta'});
            $scope.estadoAgenda.data.push({value:'C', descripcion: 'Cerrada'});
            $scope.estadoAgenda.data.push({value:'CA', descripcion: 'Cancelada'}); 
            $scope.estadoAgenda.data.push({value:'AN', descripcion: 'Anulada'});  
            $scope.estadoAgenda.data.push({value:'AP', descripcion: 'Aprobada'});   
            
            if(objFiltro){            
                $scope.estadoAgenda.current=  $filter('filter')($scope.estadoAgenda.data, {value : objFiltro.estado})[0];
            }
            else{
                
                $scope.estadoAgenda.current = $scope.estadoAgenda.data[0];
            }
                        
        }        
    }  
    
    $scope.procesoAsociado = {
      current : {}, data:[],
      getData : function() {
         
          
          if($scope.tipoAgenda.current){
              
                $rootScope.loadingVisible = true;
                agendaHttp.getProcesosAsociado({}, {proceso:$scope.procesoAgenda.current.proceso}, function(response) {  
                    
                   
                    $scope.procesoAsociado.data = response ;   
                    $scope.procesoAsociado.data.push({id:(-1), descripcion:"Todos",proceso:""});
                    $scope.procesoAsociado.data=$filter('orderBy')($scope.procesoAsociado.data, 'id');  
                    $scope.procesoAsociado.current=$scope.procesoAsociado.data[0];  
                     
                    
                  if(objFiltro){            
                    $scope.procesoAsociado.current=  $filter('filter')($scope.procesoAsociado.data, {id : objFiltro.idReferencia})[0]; 
                    $scope.responsableProceso.getData();
                    }
                    else{

                        $scope.procesoAsociado.current = $scope.procesoAsociado.data[0];
                        $scope.responsableProceso.getData();
                    }
                    
                    
                    
                    
                    $rootScope.loadingVisible = false;
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      },
        setProcesoAsociado: function() {             
           $scope.responsableProceso.getData();            
      }
    }
    
    
    //RESPONSABLE
    $scope.responsableProceso = {
      current : {}, data:[],
      getData : function() {
        
          if($scope.procesoAsociado.current){              
              
                var obj= {
                    idProceso : $scope.procesoAsociado.current.id,
                    proceso : $scope.procesoAgenda.current.proceso
                }  
              
                $rootScope.loadingVisible = true;
                agendaHttp.getResponsablesProceso({}, obj, function(response) {                      
                    
                    
                    if(response){
                        
                        $scope.responsableProceso.data = response ;   
                        $scope.responsableProceso.data.push({id:(-1), nombre:"Todos"});
                        $scope.responsableProceso.data=$filter('orderBy')($scope.responsableProceso.data, 'id'); 
                        
                        
                    }
                    else{
                         $scope.responsableProceso.data.push({id:(-1), nombre:"Todos"});                         
                    }
                    
                    if(objFiltro){            
                        $scope.responsableProceso.current=  $filter('filter')($scope.responsableProceso.data, {id : objFiltro.idResponsable})[0]; 

                    }
                    else{

                            $scope.responsableProceso.current = $scope.responsableProceso.data[0];

                        }
                    
                    
                    
                    $rootScope.loadingVisible = false;
                    
                    
                    
                }, function(faild) {
                    $rootScope.loadingVisible = false;
                      message.show("error", faild.Message);
                });
          }
      }
    }  
    
    
	//CARGAMOS DATA       
    
    $scope.agendas.procesoAsociado='Contratos:';   
        
    var date = new Date();    
    $scope.agendas.fechaInicial.value=new Date(date.getFullYear(), date.getMonth(), 1);
    $scope.agendas.fechaFinal.value=new Date(date.getFullYear(), date.getMonth() + 1, 0);
        
    $scope.procesoAgenda.getData();  
    $scope.estadoAgenda.getData();  
    
        
        if (objFiltro){          

            if(objFiltro.fechaInicial){$scope.agendas.fechaInicial.value=new Date(parseFloat(objFiltro.fechaInicial));}
            if(objFiltro.fechaFinal){$scope.agendas.fechaFinal.value=new Date(parseFloat(objFiltro.fechaFinal));}  
                setTimeout(function() { $scope.agendas.getData(); }, 1000);
            
        } 
     
 
    }
  
  
  })();