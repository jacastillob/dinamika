/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoCompromisoParticipanteController', contratoCompromisoParticipanteController);

  contratoCompromisoParticipanteController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'sidebarHttp', 'UsuarioHttp'];


  function contratoCompromisoParticipanteController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, sidebarHttp, UsuarioHttp){
    
    $scope.compromiso= {
        current : {},
        data:[],
        getData: function(){
            sidebarHttp.getcompromiso({},{},function(response) {
                $scope.compromiso.data = $filter('orderBy')(response, 'compromisoId');
                for(var i = 0; i < $scope.compromiso.data.length; i++){
                    $scope.compromiso.data[i].subcompromiso = $filter('orderBy')($scope.compromiso.data[i].subcompromiso, 'compromisoId');
                }
            }, function(faild) {
                $scope.compromiso.data = [];
                message.show("error", "No se cargaron los compromisos: " + faild.Message);
            })
        }, edit: function (item){
            var parameter = {id:0, nombreOperadora:$scope.operadora.model.nombre, empresaId:$scope.operadora.model.id};
            var modalInstance = $modal.open({
                templateUrl: 'app/views/contrato/compromisoRol_edit.html',
                controller: 'contratoCompromisoParticipanteController',
                size: 'lg',
                resolve: {
                  parameters: function () { return parameter; }
                }
            });
            modalInstance.result.then(function (parameters) {}, function (parameters) {$scope.compromiso.getData();});
        }
    }
    $scope.participantes= {
        current : {},
        data:[],
        getData: function(){
            UsuarioHttp.consultarPerfilesAll({}, {}, function(response) {
                $scope.participantes.data = response;
            }, function(faild) {
                message.show("error", "No se cargaron los perfiles : " + faild.Message);
            });
        }
    }
    
    $scope.compromiso.getData();
    $scope.roles.getData();
  }
})();