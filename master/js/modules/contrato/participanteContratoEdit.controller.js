/**=========================================================
 * Module: app.contrato.js
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('participanteContratoEditController', participanteContratoEditController);

  participanteContratoEditController.$inject = ['$scope', '$filter', '$state', '$modalInstance', 'LDataSource', 'contratoHttp',   'parameters', 'message', 'parametersOfState','$rootScope', 'REGULAR_EXPRESION'];


  function participanteContratoEditController($scope, $filter, $state, $modalInstance, LDataSource, contratoHttp, parameters, message, parametersOfState,$rootScope, REGULAR_EXPRESION) {
      
    
      
      $scope.participanteContrato = {     
          model:{
            id: 0,
            funcionarioId: 0,
            cargoId: 0,
            contratoId: 0,
            funcionario: '',
            cargo: ''
          },
		fechaReg : {
            isOpen : false,
            value:'',
            dateOptions : {
              formatYear: 'yy',
              startingDay: 1
            },
            open : function($event) {
              $scope.contrato.fechaReg.isOpen = false;
            }
          },
         save:  function(){       
             
             
                if(!$scope.funcionarios.current){message.show("warning", "Funcionario requerido");return;
                }else{$scope.participanteContrato.model.funcionarioId = $scope.funcionarios.current.id;}
          
                if(!$scope.cargo.current){message.show("warning", "Cargo requerido");return;}
                else{$scope.participanteContrato.model.cargoId= $scope.cargo.current.codigo;}	

             var _http = new contratoHttp($scope.participanteContrato.model);
             if(  $scope.participanteContrato.model.id==0){
                 
                  _http.$addParticipanteContrato(function(response){                  
                      message.show("success", "Participante creado satisfactoriamente!!");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });
             
             }
             else{         
                 
                  _http.$editParticipanteContrato(function(response){                  
                      message.show("success", "Participante actualizado satisfactoriamente");
                  }, function(faild) {
                    message.show("error", faild.Message);
                  });                 
             }             
              $modalInstance.close();
          },         
          close : function() {
              $modalInstance.dismiss('cancel');
          },
          send : function() {
              $modalInstance.close();
          }
      }
      $scope.participanteContrato.model=parameters.participanteContrato;
      
     //TIPO CONTRATO
    $scope.cargo = {
      current : {},
      data:[],
      getData : function() {
          $rootScope.loadingVisible = true;
        contratoHttp.getCargos({}, {}, function(response) {
            $scope.cargo.data = $filter('orderBy')(response, 'codigo');
            
            if(!$scope.participanteContrato.model){
                $scope.cargo.current = $scope.cargo.data[0];
                }
            else{
                $scope.cargo.current = $filter('filter')($scope.cargo.data, { codigo : $scope.participanteContrato.model.cargoId })[0];
                }
            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
            message.show("error", faild.Message);
        });
      },
      setCargo : function(){
          $scope.participanteContrato.cargoId=$scope.cargo.current.codigo;
    }}
     //FUNCIONARIOS
    $scope.funcionarios = {
      current : {}, data:[],
      getData : function() {
            $rootScope.loadingVisible = true;
            contratoHttp.getFuncionarios({}, {}, function(response) {
            
            $scope.funcionarios.data = response;            
            
            if ($scope.participanteContrato.model.funcionarioId!=0)
            {
              $scope.funcionarios.current = $filter('filter')($scope.funcionarios.data, { id : $scope.participanteContrato.model.funcionarioId })[0];   
            }
            else{
                
                $scope.funcionarios.current=$scope.funcionarios.data[0];
            }

            $rootScope.loadingVisible = false;
        }, function(faild) {
            $rootScope.loadingVisible = false;
              message.show("error", faild.Message);
        });
      },
      'setFuncionario' : function() {
          $scope.participanteContrato.funcionarioId=$scope.funcionarios.current.id;
      }
    }   
	   //CARGAMOS LOS LISTADOS	
	   $scope.cargo.getData();
       $scope.funcionarios.getData();
      //CARGAMOS LOS DATOS QUE ENVIA EL LISTADO PRINCIAPL
    
       if($scope.participanteContrato.model.fechaReg){$scope.participanteContrato.fechaReg.value=new Date(parseFloat($scope.participanteContrato.model.fechaReg));}   
    
      
  }
})();