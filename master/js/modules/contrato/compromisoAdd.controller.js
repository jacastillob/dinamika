/**=========================================================
 * Module: app.setcompromiso
 =========================================================*/

(function() {
  'use strict';

  angular
    .module('app.contrato')
    .controller('contratoCompromisoAddController', contratoCompromisoAddController);

  contratoCompromisoAddController.$inject = ['$scope', '$filter', '$state', 'LDataSource', '$modal', 'parametersOfState', 'REGULAR_EXPRESION', 'contratoHttp', 'message', '$rootScope', 'parameters', '$modalInstance'];


  function contratoCompromisoAddController($scope, $filter, $state, LDataSource, $modal, parametersOfState, REGULAR_EXPRESION, contratoHttp, message, $rootScope, parameters, $modalInstance){
    
    $scope.datosParams= parameters;    
    var datos=$scope.datosParams.detalle;    
    $scope.compromiso= {
        model : {
            nombre: '',
            tipo: '',
            tipoPeriodo: '',            
            orden: 0,
            padreId: 0,                
            subCompromiso: null,
            excel:0,
            word:0,
            pdf:0,
            fisica:0
        },
        add: function(){
            
          if(!$scope.compromiso.model.nombre){message.show("error", "Nombre Requerido..");return;}   
          if($scope.tipoPeriodo.current.value==''){message.show("error", "Período Requerido..");return;}   
          else{$scope.compromiso.model.tipoPeriodo= $scope.tipoPeriodo.current.value;}          
           
          $scope.compromiso.model.word= $scope.word.current.value;   
          $scope.compromiso.model.excel= $scope.excel.current.value;    
          $scope.compromiso.model.pdf= $scope.pdf.current.value;   
          $scope.compromiso.model.fisica= $scope.fisica.current.value;              
              
          $rootScope.loadingVisible = true;
          
        contratoHttp.newCompromiso({},$scope.compromiso.model, function (data) { 
            
                $rootScope.loadingVisible = false;   
                $scope.compromiso.close();

                }, function(faild) {
                 $rootScope.loadingVisible = false;
                 message.show("error", "No se creó el compromiso: " + faild.Message);
          })
        },
        edit: function(){
          
            
          if($scope.tipoPeriodo.current.value==''){message.show("error", "Período Requerido..");return;}   
          else{$scope.compromiso.model.tipoPeriodo= $scope.tipoPeriodo.current.value;}     
            
          $scope.compromiso.model.word= $scope.word.current.value;   
          $scope.compromiso.model.excel= $scope.excel.current.value;    
          $scope.compromiso.model.pdf= $scope.pdf.current.value;   
          $scope.compromiso.model.fisica= $scope.fisica.current.value;             
          $rootScope.loadingVisible = true;
            
          
        contratoHttp.updateCompromiso({},$scope.compromiso.model, function (data) { 
            
            $rootScope.loadingVisible = false;           
            $scope.compromiso.close();

          }, function(faild) {
            
            $rootScope.loadingVisible = false;
            message.show("error", "No se actualizo el compromiso: " + faild.Message);
            
          })
        },
        dismiss : function() {
            $modalInstance.dismiss('cancel');
        },
        close : function() {
            $modalInstance.close('close');
        }
    }
    
    $scope.excel = {
        current : {}, data:[],
        getData : function() {
            $scope.excel.data.push({value:0, descripcion: 'No'});
            $scope.excel.data.push({value:1, descripcion: 'Si'});   
        }        
    }
    
    $scope.word = {
        current : {}, data:[],
        getData : function() {
            $scope.word.data.push({value:0, descripcion: 'No'});
            $scope.word.data.push({value:1, descripcion: 'Si'});   
        }        
    }
    
    $scope.pdf = {
        current : {}, data:[],
        getData : function() {
            $scope.pdf.data.push({value:0, descripcion: 'No'});
            $scope.pdf.data.push({value:1, descripcion: 'Si'});
        }        
    }
    $scope.fisica = {
        current : {}, data:[],
        getData : function() {
            $scope.fisica.data.push({value:0, descripcion: 'No'});
            $scope.fisica.data.push({value:1, descripcion: 'Si'});
        }        
    }
     $scope.tipoPeriodo = {
        current : {}, data:[],
        getData : function() {
            $scope.tipoPeriodo.data.push({value:'', descripcion: 'Seleccione'});
            $scope.tipoPeriodo.data.push({value:'NA', descripcion: 'N/A'});  
            $scope.tipoPeriodo.data.push({value:'M', descripcion: 'Mensual'});
            $scope.tipoPeriodo.data.push({value:'T', descripcion: 'Trimestral'});
            $scope.tipoPeriodo.data.push({value:'S', descripcion: 'Semestral'});
            $scope.tipoPeriodo.data.push({value:'A', descripcion: 'Anual'});
        }        
    }
    
    $scope.excel.getData();
    $scope.word.getData();
    $scope.pdf.getData();
    $scope.fisica.getData();
    $scope.tipoPeriodo.getData();
    
    $scope.compromiso.model=datos;
      
    $scope.tipoPeriodo.current = $filter('filter')($scope.tipoPeriodo.data, {value : $scope.compromiso.model.tipoPeriodo})[0];
    $scope.excel.current = $filter('filter')($scope.excel.data, {value : $scope.compromiso.model.excel})[0];
    $scope.word.current = $filter('filter')($scope.word.data, {value : $scope.compromiso.model.word})[0];
    $scope.pdf.current = $filter('filter')($scope.pdf.data, {value : $scope.compromiso.model.pdf})[0];
    $scope.fisica.current = $filter('filter')($scope.fisica.data, {value : $scope.compromiso.model.fisica})[0];
          
  }
})();